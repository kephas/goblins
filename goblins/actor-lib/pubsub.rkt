#lang racket/base

(provide ^pubsub)

(require racket/set
         racket/match
         "../core.rkt"
         "methods.rkt")

;; TODO: Needs to be updated to support captp disconnects
(define (^pubsub bcom [subscribers (seteq)])
  (define (subscribe subscriber)
    (bcom (^pubsub bcom (set-add subscribers subscriber))))
  (define (unsubscribe subscriber)
    (bcom (^pubsub bcom (set-remove subscribers subscriber))))
  (define publish
    (make-keyword-procedure
     (lambda (kws kw-vals . args)
       (for ([subscriber subscribers])
         (keyword-apply <-np kws kw-vals subscriber args)))))
  (define publish-except
    (make-keyword-procedure
     (lambda (kws kw-vals excluding . args)
       (define subscriber-subset
         (match excluding
           [(? set?)
            (set-subtract subscribers excluding)]
           [(? list?)
            (set-subtract subscribers (list->seteq excluding))]))
       (for ([subscriber subscriber-subset])
         (keyword-apply <-np kws kw-vals subscriber args)))))
  (methods
   [subscribe subscribe]
   [unsubscribe unsubscribe]
   [publish publish]
   [publish-except publish-except]
   ;; shorthands
   [sub subscribe]
   [unsub unsubscribe]
   [pub publish]
   [pub-except publish-except]
   [(subscribers) subscribers]))

(module+ test
  (require "../vat.rkt"
           "common.rkt"
           rackunit)
  (define a-vat (make-vat))
  (define b-vat (make-vat))

  (define (^pubsub-seteq bcom [base-seteq (spawn ^seteq)])
    (define pubsub
      (spawn ^pubsub))
    (methods
     [(subscribe subscriber)
      ($ pubsub 'subscribe subscriber)]
     [(unsubscribe subscriber)
      ($ pubsub 'unsubscribe subscriber)]
     [(add val)
      ($ base-seteq 'add val)
      ($ pubsub 'publish 'add val)
      (void)]
     [(remove val)
      ($ base-seteq 'remove val)
      ($ pubsub 'publish 'remove val)]
     #:extends base-seteq))

  (define ps-set
    (a-vat 'spawn ^pubsub-seteq))

  (define b-follows-set
    (b-vat 'spawn ^seteq))

  ;; subscribe b-follows-set
  (a-vat 'call ps-set 'subscribe b-follows-set)

  (a-vat 'call ps-set 'add 'foo)
  (a-vat 'call ps-set 'add 'bar)

  (test-equal?
   "Original set has items added"
   (a-vat 'call ps-set 'data)
   (seteq 'foo 'bar))

  (sleep 0.2)

  (test-equal?
   "Following set has items added"
   (b-vat 'call b-follows-set 'data)
   (seteq 'foo 'bar))

  (a-vat 'call ps-set 'remove 'bar)

  (test-equal?
   "Original set has item removed"
   (a-vat 'call ps-set 'data)
   (seteq 'foo))

  (sleep 0.2)

  (test-equal?
   "Following set has item removed"
   (b-vat 'call b-follows-set 'data)
   (seteq 'foo))

  (a-vat 'call ps-set 'unsubscribe b-follows-set)
  (a-vat 'call ps-set 'remove 'foo)

  (test-equal?
   "Original set has last item removed"
   (a-vat 'call ps-set 'data)
   (seteq))

  (sleep 0.2)

  (test-equal?
   "Following set does not have last item removed, because it was unsubscribed"
   (b-vat 'call b-follows-set 'data)
   (seteq 'foo)))
