#lang racket/base

(provide selfish-spawn)

(require "../core.rkt")

(define (^selfish-start bcom constructor kws kw-vals args)
  (lambda (self)
    (bcom (keyword-apply constructor kws kw-vals bcom self args))))

(define selfish-spawn
  (make-keyword-procedure
   (lambda (kws kw-vals constructor . args)
     (define renamed-selfish-start
       (procedure-rename ^selfish-start (object-name constructor)))
     (define self
       (spawn renamed-selfish-start constructor kws kw-vals args))
     ;; now transition to the version with self
     ($ self self)
     self)))

(module+ test
  (require rackunit)
  (define am (make-actormap))
  (define (^narcissus bcom self stare-object)
    (lambda (how-i-feel)
      `(i-am ,self i-stare-into ,stare-object and-i-feel ,how-i-feel)))
  (define narcissus
    (actormap-run!
     am (lambda ()
          (selfish-spawn ^narcissus 'water))))
  (test-equal?
   "selfish-spawned actors know themselves"
   (actormap-peek am narcissus 'transfixed)
   `(i-am ,narcissus i-stare-into water and-i-feel transfixed)))
